<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inquiry;
use App\Comment;
use Auth ;
use DB ;
use Gate;

class InquiryController extends Controller
{


 


    function search ($keyword)
    {
       if(!Gate::allows('isAdmin'))
       {
            $data = Inquiry::where(function ($query) use($keyword) {
            $query->where('email', 'like', '%' . $keyword . '%')
            ->orWhere('message', 'like', '%' . $keyword . '%');
            })->where('user_id', '=', Auth::user()->id)
            ->paginate(2);
        }
        else
        {
            $data = Inquiry::where(function ($query) use($keyword) {
            $query->where('email', 'like', '%' . $keyword . '%')
            ->orWhere('message', 'like', '%' . $keyword . '%');
            })
            ->paginate(2); 
        }



        return view('search', ['data' => $data]);
    }


    function inquiry ($id)
    {
        $data =  Inquiry::find($id) ;
        $comments = Comment::where('inquiry_id', $id)->get();

        return view('inquiry', ['data' => $data , 'comments'=>$comments]);
    }

    function inquiries ()
    {
        if(!Gate::allows('isAdmin')){
         $data = DB::table('inquiries')->where('user_id', '=', Auth::user()->id)->paginate(3);
        }
        else
        {      
         $data = DB::table('inquiries')->paginate(3);
        }
       
        return view('inquiries', ['data' => $data]);
    }

  
    function AddInquiry (Request $request)
    {
        $request->validate([
            'name' => 'required|max:50',
            'message' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email',
        ]);
 
        $Inquiry = new Inquiry;
        $Inquiry->name = $request->name;
        $Inquiry->email = $request->email;
        $Inquiry->user_id =  Auth::user()->id;
        $Inquiry->message = $request->message;
        $Inquiry->phone = $request->phone;

         if($Inquiry->save()) 
        {
            return redirect('inquiries')->with('message', 'message sent successfully!');
            
        }
            return 'error' ;

    }

    
    
}
