<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth ;

class CommentController extends Controller
{
  
    function AddComment (Request $request)
    {
         $request->validate([
            'comment' => 'required',
        ]);
 
        $Comment = new Comment;
        $Comment->comment = $request->comment;
        $Comment->user_id =  Auth::user()->id;
        $Comment->inquiry_id = $request->inquiry_id;

         if($Comment->save()) 
        {
            return redirect('inquiry/'.$request->inquiry_id)->with('message', 'Comment sent successfully!');
            
        }
            return 'error' ;

    }
 
    
}
