<?php

namespace App\Http\Controllers;

 
use Request;
use Auth ;
use DB ;
use Gate;
use App\User;

class UserController extends Controller
{
 

    function users ()
    {
        // if(!Gate::allows('isAdmin')){
        //     abort(404,"Sorry, You can do this actions");
        // }
        $data = DB::table('users')->paginate(3);
        return view('users', ['data' => $data]);
    }

    
  
    function AddOrRemoveAdmin ()
    {
        if (Request::ajax()){
        $user =  User::find($_POST['id'] ) ;
        if ($user->system_admin == 0 ) {$user->system_admin = 1 ;$user->save(); return array(  "status"=>1, "id"=>$_POST['id'] ) ;}
        if ($user->system_admin == 1 ) {$user->system_admin = 0 ;$user->save(); return array(  "status"=>0, "id"=>$_POST['id'] );}
    }
 
 
    }

    
    
}
