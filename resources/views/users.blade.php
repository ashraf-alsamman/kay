@extends('layouts.app')

@section('content')
 <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!




@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif


    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold  my-4">users</h2>
    <!--Section description-->
    <p class="  w-responsive mx-auto mb-5"> you can manage user status to add admin or remove .</p>

        <div class="container">
    @foreach ($data as $raw)
     
        @if ($raw->system_admin == 0)
         <button  class ="AddAdmin btn btn-success" id ={{ $raw->id }} ><span id=user{{ $raw->id }}>Add Admin </span></button>
        @else
        <button  class ="AddAdmin btn btn-danger" id ={{ $raw->id }} ><span id=user{{ $raw->id }}>Remove Admin</span></button>
        @endif
          
        {{ $raw->name }} {{ $raw->email }}
        <hr>
      
    @endforeach
</div>

{{ $data->links() }}


 




<script type="text/javascript">




$(document).ready(function(){




$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
});
$(".AddAdmin").click(function() {

        // e.preventDefault();
  
 
 
    $.ajax({
        type: 'post',
        url:  'AddOrRemoveAdmin',
        data: { id:  this.id},
        // dataType: 'json',
        success: function(data) {
            // console.log('success: '+data);
           if (data.status == 0 ){ 
               $("#user"+data.id).html('Add Admin');
               $('#'+data.id).removeClass( "btn-danger" ).addClass( "btn-success" );
               }
           if (data.status == 1 ){
                $("#user"+data.id).html('Remove Admin');
                $('#'+data.id).removeClass( "btn-success" ).addClass( "btn-danger" );
                }
 

        },
        error: function(data) {
            var errors = data.responseJSON;
            console.log(errors);
        }
    });
});

 

 

});



</script>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
