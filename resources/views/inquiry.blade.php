@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

 

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif

 
  

        <div class="container">
 
       Name : {{ $data->name }} 
        <hr>
        Email : {{ $data->email }}
       <hr>
       Phone : {{ $data->phone }}
       <hr>
       message : {{ $data->message }}
  
</div>
<hr>
<h2>Comments</h2>
<hr>
    @foreach ($comments as $raw)
        {{  date("d M Y", strtotime($raw->created_at)) }} <br>
        {{ $raw->comment }}  <hr>
    @endforeach

 
        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
        @can('isAdmin')
            <form id="contact-form" name="contact-form" action="/AddComment" method="POST">

 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="inquiry_id" value={{request()->route()->parameters['id']}}>
                {{ csrf_field() }}
                


                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="comment" name="comment" rows="3" class="form-control md-textarea"></textarea>
                            <label for="message">Your Comment</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->
                <div class="text-center text-md-left">
                <a class="btn btn-primary" style="color: #fff " onclick="document.getElementById('contact-form').submit();">Send</a>
                </div>
            </form>
            @endcan
            @if(  !Gate::check('isAdmin'))
              <h3>you must be admin to add comments</h3>
            @endif





                </div>
            </div>
        </div>
    </div>
</div>
@endsection
