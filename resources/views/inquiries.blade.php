@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('message'))
                    <div class="alert alert-success">
                    {{ session('message') }}
                    </div>
                    @endif
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-10 col-lg-8">
                                 <div class="card-body row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <i class="fas fa-search h4 text-body"></i>
                                    </div>
                                    <!--end of col-->
                                    <div class="col">
                                        <input class="form-control form-control-lg form-control-borderless" type="search" placeholder="Search topics or keywords" value=''  name="search" id="search_value">
                                    </div>
                                    <!--end of col-->
                                    <div class="col-auto">
                                        <button  id="search" class="btn btn-lg btn-success"  >Search</button>
                                    </div>
                                    <!--end of col-->
                                </div>
                         </div>
                        <!--end of col-->
                    </div>

 

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 


    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold  my-4">inquiries</h2>
    <!--Section description-->
 

        <div class="container">
    @foreach ($data as $raw)
        <a href='{!! url('/inquiry/'.$raw->id); !!}'>  {{ $raw->name }}  | {{ $raw->email }}</a>
        <hr>
    @endforeach
    @if($data->isEmpty())
    <h3>No results found<h3>
    @endif
</div>

{{ $data->links() }}




 

<script>
$(document).ready(function(){

$('#search').click(function(event){
    event.preventDefault();
    var search_value = document.getElementById("search_value").value
   if (search_value == ''){
    window.location.href='/inquiries';
   }else{
      window.location.href='/search/'+search_value; 
   }
  
      
})
});

</script>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
